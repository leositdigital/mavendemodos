package principal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App{
	private static Connection conn = null;
	private static String usuario = "USERTATA";
	private static String contrasenia = "1234567";
	private static String URL ="jdbc:oracle:thin:@localhost:1521:XE";
	
    public static void main( String[] args ) {
        System.out.println( "Operación consulta DB con JDBC como dependencia" );
        Connection con = getConnection();
        
        String query = "SELECT NOMBRE,EDAD,DEPARTAMENTO,ANTIGUEDAD FROM USERAXITY.EMPLEADO";
        
        Statement stmt;
		try {
			
			stmt = con.createStatement();
			ResultSet response = stmt.executeQuery(query);
			List<String> listEmpleados = new ArrayList();
			while(response.next()) {
				String nombre = response.getString("NOMBRE");
				String edad = response.getString("EDAD");
				String depto = response.getString("DEPARTAMENTO");
				String antiguedad = response.getString("ANTIGUEDAD");				
				System.out.println("Tabla Empleado : Nombre: "+nombre+" Edad: "+edad+" Departament: "+depto+ " Antiguedad :"+ antiguedad);
				listEmpleados.add(nombre);
				
			}
			
		} catch (Exception e) {
			System.out.println("Exception ......  de base de datos");			
		}     
    }
    
    public static Connection getConnection() {
    	
    	try {
			Class.forName("oracle.jdbc.OracleDriver");		
			conn= DriverManager.getConnection(URL,usuario,contrasenia);
			if(conn != null) {
				System.out.println("Conexión exitosa");
			}
		} catch (Exception e) {
			System.out.println("Conexión incorrecta");
			e.printStackTrace();
		}
    	return conn;
    }
}
